﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Konyisoft.Driving
{
	public class UIManager : Singleton<UIManager>
	{
		#region Fields and properties

		public Text distanceText;
		public Text speedText;
		public Text debugText;
		
		public bool showDebug;

		#endregion
		
		#region Mono methods

		void Awake()
		{
			SetDistanceText(string.Empty);
			SetSpeedText(string.Empty);
			SetDebugText(string.Empty);
		}

		void Update()
		{
			float distance = PlayerManager.Instance.DistanceTraveled * Constants.KmMultiplier;
			SetDistanceText(string.Format("{0:0.0} km", Math.Truncate(distance * 10) / 10));

			float speed = Mathf.Round(PlayerManager.Instance.CurrentSpeed * Constants.KmPerHourMultiplier);
			SetSpeedText(string.Format("{0} km/h", speed));
			
			if (showDebug)
				SetDebugText(PlayerManager.Instance.DebugString);
		}
		
		#endregion

		#region Public methods

		public void SetDistanceText(string text)
		{
			if (distanceText != null)
				distanceText.text = text;
		}

		public void SetSpeedText(string text)
		{
			if (speedText != null)
				speedText.text = text;
		}
		
		public void SetDebugText(string text)
		{
			if (debugText != null)
				debugText.text = text;
		}

		#endregion
	}
}
