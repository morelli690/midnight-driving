﻿namespace Konyisoft.Driving
{
	public static class Constants
	{
		public const float KmPerHourMultiplier = 18f / 5f;
		public const float KmMultiplier        = 0.001f;
	}
}
