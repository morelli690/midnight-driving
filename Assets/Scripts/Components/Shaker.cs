﻿using UnityEngine;

namespace Konyisoft.Driving
{
	public class Shaker : GameBehaviour
	{
		#region Fields and properties
		
		public float intensity = 0.3f;
		public float decay = 0.002f;

		Vector3 origLocalPosition;
		Quaternion origLocalRotation;
		float currentIntensity;
		bool isShaking;
		
		#endregion
		
		#region Mono methods
				
		void Update()
		{
			if (isShaking)
			{
				if (currentIntensity > 0f)
				{
					transform.localPosition = origLocalPosition + Random.insideUnitSphere * currentIntensity;
					transform.localRotation = origLocalRotation * Quaternion.Euler(
						Random.Range(-currentIntensity, currentIntensity),
						Random.Range(-currentIntensity, currentIntensity),
						Random.Range(-currentIntensity, currentIntensity)
					);
					currentIntensity -= decay * Time.deltaTime;
				}
		
				if (currentIntensity <= 0)
				{
					RestoreTransform();
					isShaking = false;
				}		
			}
		}
		
		#endregion
		
		#region Public methods
	 
		public void Shake()
		{
			if (!isShaking)
			{
				BackupTransform();
				currentIntensity = intensity;
				isShaking = true;
			}
		}
		
		public void Stop()
		{
			isShaking = false;
			RestoreTransform();
		}
		
		#endregion
		
		#region Private methods
		
		void BackupTransform()
		{
			origLocalPosition = transform.localPosition;
			origLocalRotation = transform.localRotation;
		}
		
		void RestoreTransform()
		{
			transform.localPosition = origLocalPosition;
			transform.localRotation = origLocalRotation;
		}
		
		
		#endregion
	}
}
