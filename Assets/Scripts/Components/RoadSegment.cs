﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Konyisoft.Driving
{
	public class RoadSegment : GameBehaviour
	{
		#region Classes and structs

		[Serializable]
		struct PointData
		{
			public Vector3 left;
			public Vector3 center;
			public Vector3 right;

			public PointData(Vector3 left, Vector3 center, Vector3 right)
			{
				this.left = left;
				this.center = center;
				this.right = right;
			}
		}

		#endregion

		#region Fields and properties

		public float CurveRadius
		{
			get { return Mathf.Abs(curveRadius); }
		}

		public float CurveAngle
		{
			get { return curveAngle; }
		}

		public float CurvLength
		{
			get { return curveLength; }
		}
		
		public float CurveDirection
		{
			get { return curveDirection; }
		}

		public Vector3 StartDirection
		{
			get 
			{
				return points.Count > 1 ? (points[1].center - points[0].center).normalized : Vector3.forward;
			}
		}
		
		public float minCurveRadius, maxCurveRadius;
		public int minCurveSegmentCount, maxCurveSegmentCount;
		public float pointDistance;
		public float width;
		[Range(0f, 1f)]
		public float hillChance;
		public float hillMinHeight, hillMaxHeight;
		public List<AnimationCurve> hillCurves = new List<AnimationCurve>();

		float curveRadius;
		int curveSegmentCount;
		float curveAngle;
		float curveLength;
		float curveDirection;
		float baseY;
		List<PointData> points;
		RoadSegment segmentToAlign;

		MeshFilter meshFilter;
		MeshRenderer meshRenderer;
		MeshCollider meshCollider;
		
		#endregion
		
		#region Mono methods

		void Awake()
		{
			meshFilter = GetComponent<MeshFilter>();
			if (meshFilter == null)
				meshFilter = gameObject.AddComponent<MeshFilter>();

			meshRenderer = GetComponent<MeshRenderer>();
			if (meshRenderer == null)
				meshRenderer = gameObject.AddComponent<MeshRenderer>();

			meshCollider = GetComponent<MeshCollider>();
			if (meshCollider == null)
				meshCollider = gameObject.AddComponent<MeshCollider>();
		}
		
		#endregion
		
		#region Public methods

		public void Generate(RoadSegment segmentToAlign)
		{
			this.segmentToAlign = segmentToAlign;
			
			// Randomize radius and segment count
			curveRadius = UnityEngine.Random.Range(minCurveRadius, maxCurveRadius);
			curveSegmentCount = UnityEngine.Random.Range(minCurveSegmentCount, maxCurveSegmentCount + 1);
			
			// Set curve to the opposite direction
			if (segmentToAlign != null)
				curveRadius *= -segmentToAlign.curveDirection;

			// Get the last Y position from aligned segment
			if (segmentToAlign != null)
				baseY = segmentToAlign.points[segmentToAlign.points.Count - 1].center.y;

			// Create road points (curve on XZ axes)
			SetPoints();

			// Create hills (Y)
			if (hillCurves.Count > 0 && UnityEngine.Random.Range(0f, 1f) < hillChance)
				SetHillCurve();

			Mesh mesh = GenerateMesh();
			if (mesh != null)
			{
				// Attach mesh
				meshFilter.mesh = mesh;
				// Update the collider
				meshCollider.sharedMesh = null;
				meshCollider.sharedMesh = mesh;
			}

			Align();
		}

		public void SetDefaultValues()
		{
			points.Clear();
			segmentToAlign = null;
			curveAngle = 0;
			curveDirection = 0;
			meshFilter.mesh = null;
			meshCollider.sharedMesh = null;
		}
		
		#endregion
		
		#region Private methods

		void SetPoints()
		{
			points = new List<PointData>();

			for (int segment = 0; segment <= curveSegmentCount; segment++)
			{
				PointData point = new PointData(
					GetPointOnCurve(segment, curveRadius + width / 2f),
					GetPointOnCurve(segment, curveRadius),
					GetPointOnCurve(segment, curveRadius - width / 2f)
				);
				points.Add(point);
			}

			// Values for the next segment to align with
			curveAngle = (pointDistance / curveRadius) * curveSegmentCount * (360f / (2f * Mathf.PI));
			curveLength = curveRadius * curveAngle * Mathf.Deg2Rad;
			curveDirection = curveRadius >= 0 ? 1f : -1f;
		}

		void SetHillCurve()
		{
			// Get a random curve and random height
			AnimationCurve hillCurve = hillCurves[UnityEngine.Random.Range(0, hillCurves.Count)];
			float hillHeight = UnityEngine.Random.Range(hillMinHeight, hillMaxHeight);
		
			if (hillCurve != null)
			{
				for (int i = 0; i < points.Count; i++)
				{
					float normalized = MathUtils.Normalize(i, 0, curveSegmentCount);
					float y = hillCurve.Evaluate(normalized);
					PointData point = points[i];
					point.left.y = point.center.y = point.right.y += y * hillHeight;
					points[i] = point;
				}			
			}
		}

		Vector3 GetPointOnCurve(int segment, float radius)
		{
			float pd = pointDistance * ((1f / curveRadius) * radius);
			float u = segment * (pd / radius);
			return new Vector3(radius * Mathf.Sin(u), baseY, radius * Mathf.Cos(u));
		}

		void Align()
		{
			if (segmentToAlign != null)
			{
				transform.SetParent(segmentToAlign.transform, false);
				transform.localPosition = Vector3.zero;
				transform.localRotation = Quaternion.Euler(0, segmentToAlign.curveAngle, 0);
				transform.Translate(0, 0, segmentToAlign.curveRadius - curveRadius);
				transform.SetParent(segmentToAlign.transform.parent);
				transform.localScale = Vector3.one;
			}
		}

		Mesh GenerateMesh()
		{
			// At least 2 points are needed
			if (points.Count < 2)
				return null;

			/*
				Indexing triangles w/ vertices:

				i+5---i+4---i+3
				|  /  |  /  |
				i+2---i+1---i+0
				=============
				8 --- 7 --- 6
				|  /  |  /  |
				5 --- 4 --- 3
				=============
				5 --- 4 --- 3
				|  /  |  /  |
				2 --- 1 --- 0

				1st triangle (0 -> 1 -> 3)
				2nd triangle (4 -> 3 -> 1)
				3rd triangle (1 -> 2 -> 4)
				4th triangle (5 -> 4 -> 2)
			*/

			// Vertices and triangles
			int[] indicies = { 0, 1, 3, 4, 3, 1, 1, 2, 4, 5, 4, 2 };
			List<Vector3> vertices = new List<Vector3>();
			List<int> triangles = new List<int>();
			int index = 0;

			PointData point = points[0];

			vertices.Add(point.right);
			vertices.Add(point.center);
			vertices.Add(point.left);

			// Define vertices and tris: 4 triangles (6 vertices) for 2 quads
			for (int i = 0; i < points.Count - 1; i++)
			{
				point = points[i + 1];

				// Top vertices will be the next triangles' bottom vertices
				vertices.Add(point.right);
				vertices.Add(point.center);
				vertices.Add(point.left);

				// Triangles
				for (int j = 0; j < indicies.Length; j++)
				{
					triangles.Add(index + indicies[j]);
				}

				index += 3;
			}

			// UVs / 2 quads
			Vector2[] uv = new Vector2[vertices.Count];
			int v = 0;
			for (int i = 0; i < uv.Length; i += 3)
			{
				uv[i + 0] = new Vector2(1f, v);
				uv[i + 1] = new Vector2(0.5f, v);
				uv[i + 2] = new Vector2(0, v);
				v++;
			}

			// Example:
			// uv[0]  = new Vector2(1,   0);
			// uv[1]  = new Vector2(0.5, 0);
			// uv[2]  = new Vector2(0,   0);
			// uv[3]  = new Vector2(1,   1);
			// uv[4]  = new Vector2(0.5, 1);
			// uv[5]  = new Vector2(0,   1);
			// uv[6]  = new Vector2(1,   2);
			// uv[7]  = new Vector2(0.5, 2);
			// uv[8]  = new Vector2(0,   2);
			// etc.

			// Create the mesh
			Mesh mesh = new Mesh();
			mesh.vertices = vertices.ToArray();
			mesh.triangles = triangles.ToArray();
			mesh.uv = uv;

			// Recalculate
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();

			return mesh;
		}
		
		#endregion
	}
}
